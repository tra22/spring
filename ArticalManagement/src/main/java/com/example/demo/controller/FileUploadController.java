package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.github.javafaker.File;

@Controller
@PropertySource("classpath:ams.properties")
public class FileUploadController {
   @GetMapping("/upload")
   public String upload() {
	   return "upload";
   }
   
   @Value("${file.upload.server.path}")
   String serverPath;
   @PostMapping("/upload")
   public String fileUpload(@RequestParam("file") List<MultipartFile > files) {
	   System.out.println("Runing this file/.......");
	   if(!files.isEmpty()) {
		   files.forEach(file->{
			   String filename=UUID.randomUUID().toString()+file.getOriginalFilename();
			   try {
				Files.copy(file.getInputStream(),Paths.get(serverPath, filename));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   });
		  
	   }
	   return "redirect:/upload";
   }
}
