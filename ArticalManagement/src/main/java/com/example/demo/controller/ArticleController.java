package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Article;
import com.example.demo.service.ArticleService;
import com.example.demo.service.ArticleServiceImp;
import com.example.demo.service.categoryservice.CategoryService;
 
 
@Controller
public class ArticleController {
	@Value("${file.upload.server.path}")
    private String serverPath;
	 
	@Autowired
	CategoryService categoryService;
	@Autowired
	private ArticleService articleService;
    @GetMapping("/article")
   public String article(ModelMap map) {
	   List<Article> articles= articleService.findAll();
	   System.out.println(articles);
	   map.addAttribute("articles", articles);
	   return ("article");
       }
   
	   @GetMapping("/add")
	   public String Add(ModelMap map) {
		   map.addAttribute("article", new Article());
		   map.addAttribute("categories",categoryService.findAll());
		   map.addAttribute("formAdd", true);
		   return "add";
	   }
	   
	   @GetMapping("/delete/{id}")
	   public String delete(@PathVariable("id") int id) {
		   articleService.delete(id);
		   return "redirect:/article";
	   }
	   
	   @PostMapping("/add")
	   public String Save( @Valid @ModelAttribute Article article,BindingResult result,ModelMap map,@RequestParam ("picture") MultipartFile image) {
		    if(result.hasErrors()) {
		    	   map.addAttribute("article", article);
		    	   map.addAttribute("categories",categoryService.findAll());
				   map.addAttribute("formAdd", true);
				   return "add"; 
		    }
		  if(image.isEmpty()) {
			  return "add";
		  }else {
			  try {
				Files.copy(image.getInputStream(), Paths.get(serverPath));
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	        
		    
	       article.setImage("/image/"+image.getOriginalFilename());
		   article.setCategory(categoryService.findone(article.getCategory().getId()));
		   article.setCreatedDate(new Date().toString());
		   articleService.add(article);
		 
		   return "redirect:/article";
	   }
	   
	   @GetMapping("/update/{id}")
	   public String update(@PathVariable int id , ModelMap map) {
		   map.addAttribute("article", articleService.findOne(id) );
		   map.addAttribute("categories",categoryService.findAll());
		   map.addAttribute("formAdd", false);
		   return "add";
	   }
	   
	   @PostMapping("/update")
	   public String SaveUpdate(@ModelAttribute Article article ) {
		   
		    article.setCategory(categoryService.findone(article.getCategory().getId()));
		    articleService.update(article);
		   return "redirect:/article";
	   }
}
