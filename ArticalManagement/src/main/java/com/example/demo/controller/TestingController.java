package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.repository.CategoryNewRepo;

@Controller
public class TestingController {
	@Autowired 
	private CategoryNewRepo repo;
	
	@GetMapping("/test")
	@ResponseBody
	public String test() {
		 repo.showCategory();
		return "test";
	}
}
