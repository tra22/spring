package com.example.demo.service.categoryservice;

import java.util.List;

import com.example.demo.model.Category;

public interface CategoryService {
   List<Category> findAll();
   Category findone(int id);
}
   
