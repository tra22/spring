package com.example.demo.service.categoryservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Category;
import com.example.demo.repository.category.CategoryRepository;
@Service
public class CategoryServiceImp implements CategoryService {
	@Autowired
	 public CategoryRepository categoryRepo;
	@Override
	public List<Category> findAll() {
	   return categoryRepo.findall();
	}
	@Override
	public Category findone(int id) {
		return categoryRepo.findone(id);
	}
}
