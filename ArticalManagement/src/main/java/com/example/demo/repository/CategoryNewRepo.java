package com.example.demo.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryNewRepo {
  @Autowired
  private  DataSource dataSource;
    public void showCategory() {
	    try {
			Connection con =dataSource.getConnection();
			Statement statement=con.createStatement();
			String sql="select * from tb_categories";
			ResultSet rs= statement.executeQuery(sql);
			while (rs.next()) {
				 System.out.println("ID: "+rs.getInt(1)+"Name: "+rs.getString(2));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
}
