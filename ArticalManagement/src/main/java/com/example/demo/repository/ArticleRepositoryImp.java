package com.example.demo.repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import com.github.javafaker.Faker;

import ch.qos.logback.core.joran.action.Action;
// don't @Repository
public class ArticleRepositoryImp implements ArticleRepository {
     private List<Article> articles=new ArrayList<>();
     Faker f=new Faker();
     Date date = new Date();
     SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
     public ArticleRepositoryImp() {
    	 for(int i=1;i<11;i++) {
            articles.add(new Article(i,f.book().title(),f.book().title(),f.book().title(),df.format(date),new Category(4,"database"),f.internet().image(100,100,false,null)));
            }
    	 
	}
	@Override
	public void add(Article article) {
	   articles.add(article);
		
	}

	@Override
	public Article findOne(int id) {
		 for(Article article: articles) {
			 if(article.getId()==id)
				 return article;
		 }
		return null;
	}

	@Override
	public List<Article> findAll() {
		// TODO Auto-generated method stub
		return articles;
	}
	@Override
	public void delete(int id) {
		 for(Article article: articles) {
			 if(article.getId()==id) {
				  articles.remove(article);
			    return;
			 }
	 }	
	}
	  @Override
	public void update(Article article) {
		   
		  for(int i=0;i<articles.size();i++) {
				  if(articles.get(i).getId()==article.getId()) {
					  articles.get(i).setId(article.getId());
					  articles.get(i).setName(article.getName());
					  articles.get(i).setCategory(article.getCategory());
					  articles.get(i).setDescription(article.getDescription());
					  articles.get(i).setAuthur(article.getAuthur());
					  articles.get(i).setImage(article.getImage());
					  return;
				  }	  
		 }	
		
	}
}
