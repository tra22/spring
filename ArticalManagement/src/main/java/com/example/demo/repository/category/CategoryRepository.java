package com.example.demo.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Category;
 @Repository
public interface CategoryRepository {
	@Select("SELECT id,name from tb_categories ORDER BY id ASC")
    public List<Category> findall();
	@Select("Select id,name from tb_categories WHERE id=#{id}")
    public Category findone(int id);
}
