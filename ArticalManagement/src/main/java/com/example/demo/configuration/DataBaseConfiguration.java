package com.example.demo.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DataBaseConfiguration {
  @Bean
  @Profile("production")
  public DataSource dataSource() {
	  DriverManagerDataSource driver =new DriverManagerDataSource();
	 driver.setDriverClassName("org.postgresql.Driver");
	 driver.setUrl("jdbc:postgresql://localhost:5432/stockdb");
	 driver.setUsername("postgres");
	 driver.setPassword("pheaktra22");
    return driver;
  }
  @Bean
  @Profile("memory")
  public DataSource data() {
	 EmbeddedDatabaseBuilder db=new EmbeddedDatabaseBuilder();
	 db.setType(EmbeddedDatabaseType.H2);
	 db.addScript("sql/table.sql");
	 db.addScript("sql/data.sql");
    return db.build();
  }
}
