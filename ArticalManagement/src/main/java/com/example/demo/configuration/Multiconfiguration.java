package com.example.demo.configuration;

import java.util.Locale;

import org.apache.tomcat.util.descriptor.LocalResolver;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
	public class Multiconfiguration extends WebMvcConfigurerAdapter{
	  @Bean
	public  LocaleResolver localeResolver(){
	    SessionLocaleResolver l = new  SessionLocaleResolver();
	     l.setDefaultLocale(Locale.US);
	     return l;
	  }
	  @Bean
	public  LocaleChangeInterceptor localeChangeInterceptor() {
		  LocaleChangeInterceptor lc=new LocaleChangeInterceptor();
		  lc.setParamName("lang"); 
		  return lc;
	  }
	  
	  @Override
	public void addInterceptors(InterceptorRegistry registry) {
		 
		 registry.addInterceptor(this.localeChangeInterceptor());
	}
	  @Bean
	  public MessageSource messageSource() {
		  ResourceBundleMessageSource msg=new ResourceBundleMessageSource();
		  msg.setBasename("languages/messages");
		  return msg;
	  }
}
