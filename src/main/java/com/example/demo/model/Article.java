package com.example.demo.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

public class Article {
  @NotNull
  private int id;
  @NotBlank
  private String name;
  @NotBlank
  private String description;
  @NotBlank
  private String authur; 
  private String createdDate;
  //String picture;
  public Category category;
  private String image;
  
@Override
public String toString() {
	return "Article [id=" + id + ", name=" + name + ", description=" + description + ", authur=" + authur
			+ ", createdDate=" + createdDate + ", category=" + category + ", image=" + image + "]";
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getAuthur() {
	return authur;
}
public void setAuthur(String authur) {
	this.authur = authur;
}
public String getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(String createdDate) {
	this.createdDate = createdDate;
}
public Category getCategory() {
	return category;
}
public void setCategory(Category category) {
	this.category = category;
}
public String getImage() {
	return image;
}
public void setImage(String image) {
	this.image = image;
}
public Article(@NotNull int id, @NotBlank String name, @NotBlank String description, @NotBlank String authur,
		String createdDate, Category category, String image)
{
	super();
	this.id = id;
	this.name = name;
	this.description = description;
	this.authur = authur;
	this.createdDate = createdDate;
	this.category = category;
	this.image = image;
}
public Article() {
	super();
	// TODO Auto-generated constructor stub
}

}
  