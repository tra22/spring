package com.example.demo.model;

import org.hibernate.validator.constraints.NotBlank;

public class Category {
  int id;
  @NotBlank
  String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Category() {
		super();
		 
	}
}
