package com.example.demo.controller;
import javax.validation.Valid;

import org.h2.util.New;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.Category;
import com.example.demo.model.NewRepository;
import com.example.demo.service.categoryservice.CategoryService; 

@Controller
public class CategoryController {
	
	private CategoryService categoryService;
	
	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	 @GetMapping("/category")
	 public String cate(ModelMap map) 
	 {
		 map.addAttribute("category", new Category());
		 map.addAttribute("categories",categoryService.findAll());
		 map.addAttribute("formAdd",true);
		 return "category/category";
     }
	 @PostMapping("/addCategory")
	 public String save(@ModelAttribute Category category ,ModelMap map ) {
		 categoryService.addCategory(category);
		 map.addAttribute("formAdd",true);
		 return "redirect:/category";
	 }
	 
	 @GetMapping("/deleteCategory/{id}")
	 public String deleteCate(@PathVariable ("id") int id) {
		 categoryService.delete(id);
		 return "redirect:/category";
	 }
	 
	 
	 @GetMapping("/updateCategory/{id}")
	 public String updateCategory(@PathVariable int id, ModelMap map)
	 {
		 map.addAttribute("category",categoryService.findOneCategory(id));
		 map.addAttribute("categories", categoryService.findAll());
		 map.addAttribute("formAdd",false);
		 return "category/category";
	 }
	 
	 @PostMapping("/updateCategory")
	 public String updateCategory(ModelMap map,@ModelAttribute Category category)
	 {
		 map.addAttribute("category", categoryService.findAll());
		 categoryService.updateCategory(category);
		 map.addAttribute("formAdd",true);
		 return "redirect:/category";
	 }
	 
	 
	 /*@GetMapping("/updateCategory/{id}")
	 public String updateCategory(@PathVariable int id,ModelMap map) {
		 map.addAttribute("category", categoryService.findOneCategory(id));
		 return "category/category";
	 }*/
}