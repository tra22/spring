package com.example.demo.service.categoryservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Category;
import com.example.demo.repository.category.CategoryRepository;
@Service
public class CategoryServiceImp implements CategoryService {
 
	private CategoryRepository categoryRepository;
	@Autowired
	public void setCategoryRepository(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}
	
	@Override
	public void addCategory(Category category) {
		categoryRepository.addCategory(category);	
	}

	@Override
	public Category findOneCategory(int id) {	
		return categoryRepository.findOneCategory(id);
	}

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	 @Override
	public void updateCategory(Category category) {
		 categoryRepository.updateCategory(category);
	}
	 @Override
	public void delete(int id) {
		 categoryRepository.deleteCategory(id);
	}

}
