package com.example.demo.service.categoryservice;

import java.util.List;

import com.example.demo.model.Category;

public interface CategoryService {
     List<Category> findAll();
     void addCategory(Category category);
	  Category findOneCategory(int id);
	 void updateCategory(Category category);
	 void delete(int id);
}
   
