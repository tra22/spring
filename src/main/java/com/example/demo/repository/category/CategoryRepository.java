package com.example.demo.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Category;

@Repository
public interface CategoryRepository {

	@Insert("insert into tb_categories(name) values(#{name})")
	public void addCategory(Category category);

	@Select("select id, name from tb_categories where id=#{id}")
	@Results({ @Result(property = "id", column = "id"), @Result(property = "name", column = "name") })
	public Category findOneCategory(Integer id);

	@Select("select id, name from tb_categories")
	@Results({ @Result(property = "id", column = "id"), @Result(property = "name", column = "name") })
	public List<Category> findAll();

	@Update("update tb_categories set name=#{name} where id=#{id}")
	public void updateCategory(Category category);

	@Delete("delete from tb_categories where id=#{id}")
	public void deleteCategory(int id);
}
