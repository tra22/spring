package com.example.demo.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;
@Repository
public interface ArticleRepository {
	@Insert("insert into tb_article(title,description,author,created_date,image,category_id) Values(#{name},#{description},#{authur},#{createdDate},#{image},#{category.id})")
	public void add(Article article);
	@Select("Select a.id, a.title,a.description,a.author,a.created_date,a.image,a.category_id,c.name as category_name from tb_article a INNER JOIN tb_categories c ON a.category_id=c.id WHERE a.id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="name",column="title"),
		@Result(property="description",column="description"),
		@Result(property="authur",column="author"),
		@Result(property="createdDate",column="created_date"),
		@Result(property="image",column="image"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="category_name")
		
	})
	public Article findOne(int id);
	@Select("Select a.id, a.title,a.description,a.author,a.created_date,a.image,a.category_id,c.name as category_name from tb_article a INNER JOIN tb_categories c ON a.category_id=c.id ORDER BY a.id ASC")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="name",column="title"),
		@Result(property="description",column="description"),
		@Result(property="authur",column="author"),
		@Result(property="createdDate",column="created_date"),
		@Result(property="image",column="image"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="category_name")
		
	})
	public List<Article> findAll();
	@Delete("Delete from tb_article where id =#{id}")
	public void delete(int id);
	@Update("update tb_article set title=#{name},description=#{description},author=#{authur}, category_id=#{category.id} where id= #{id} ")
	public void update(Article article);
}
