  
 CREATE TABLE tb_categories(
  id int PRIMARY KEY auto_increment,
  name VARCHAR not null
 );
 
 
 CREATE TABLE tb_article(
 id int PRIMARY KEY auto_increment,
 title VARCHAR NOT NULL ,
 description VARCHAR not null,
 author VARCHAR not null,
 created_date VARCHAR not null,
 image VARCHAR not null,
 category_id INT REFERENCES tb_categories(id) ON DELETE CASCADE ON UPDATE CASCADE 
 );